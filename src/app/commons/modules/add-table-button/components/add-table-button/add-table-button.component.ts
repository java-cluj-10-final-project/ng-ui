import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-table-button',
  templateUrl: './add-table-button.component.html',
  styleUrls: ['./add-table-button.component.css']
})
export class AddTableButtonComponent implements OnInit {

  @Input() type: string;
  @Input() meterId: number;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public add(): void {

    switch (this.type.toLowerCase()) {
      case 'info': {
        this.router.navigateByUrl(`/meter/${this.meterId}/addInfo`);
        break;
      }
      case 'value': {
        this.router.navigateByUrl(`/meter/${this.meterId}/addValue`);
        break;
      }
    }
  }
}
