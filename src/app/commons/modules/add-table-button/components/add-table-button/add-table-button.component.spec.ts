import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTableButtonComponent } from './add-table-button.component';

describe('AddTableButtonComponent', () => {
  let component: AddTableButtonComponent;
  let fixture: ComponentFixture<AddTableButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTableButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTableButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
