import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddTableButtonComponent } from './components/add-table-button/add-table-button.component';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [AddTableButtonComponent],
  imports: [
    MatButtonModule,
    MatIconModule,
    CommonModule
  ],
  exports: [
    AddTableButtonComponent
  ]
})
export class AddTableButtonModule { }
