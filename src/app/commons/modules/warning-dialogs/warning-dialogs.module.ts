import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteWarningDialogComponent } from './components/delete-warning-dialog/delete-warning-dialog.component';

import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [DeleteWarningDialogComponent],
  imports: [
    MatButtonModule,
    CommonModule
  ],
  exports: [
    DeleteWarningDialogComponent
  ]
})
export class WarningDialogsModule { }
