import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MeterService } from 'src/app/modules/meters/services/meter.service';

import DataType from '../../../../../constants/DataType';

@Component({
  selector: 'app-delete-warning-dialog',
  templateUrl: './delete-warning-dialog.component.html',
  styleUrls: ['./delete-warning-dialog.component.css']
})
export class DeleteWarningDialogComponent implements OnInit {

  constructor(
    private service: MeterService,
    public dialogRef: MatDialogRef<DeleteWarningDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {
      message: string,
      type: string,
      deleteId: number
    }
  ) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onClick(): void {
    if (this.data.type.toLowerCase() === DataType.INFO) {
      this.service.deleteMeterInfo(this.data.deleteId)
        .subscribe(() => {
          console.log(`Meter info with id ${this.data.deleteId} was deleted`);
        },
          (err: any) => {
            console.log(`error: ${err}`);
          });
    } else if (this.data.type.toLowerCase() === DataType.VALUE) {
      this.service.deleteMeterValue(this.data.deleteId)
        .subscribe(() => {
          console.log(`Meter value with id ${this.data.deleteId} was deleted`);
        },
          (err: any) => {
            console.log(`error: ${err}`);
          });
    } else if (this.data.type.toLowerCase() === DataType.METER) {
      this.service.deleteMeter(this.data.deleteId)
        .subscribe(() => {
          console.log(`Meter with id ${this.data.deleteId} was deleted`);
        },
          (err: any) => {
            console.log(`error: ${err}`);
          });
    }
    this.dialogRef.close();
  }
}
