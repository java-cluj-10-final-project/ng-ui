import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditRemoveTableButtonsComponent } from './components/edit-remove-table-buttons/edit-remove-table-buttons.component';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';

import { RemoveTableButtonComponent } from './components/remove-table-button/remove-table-button.component';

@NgModule({
  declarations: [
    EditRemoveTableButtonsComponent,
    RemoveTableButtonComponent
  ],
  imports: [
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    CommonModule
  ],
  exports: [
    EditRemoveTableButtonsComponent,
    RemoveTableButtonComponent
  ]
})
export class EditRemoveTableButtonsModule { }
