import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveTableButtonComponent } from './remove-table-button.component';

describe('RemoveTableButtonComponent', () => {
  let component: RemoveTableButtonComponent;
  let fixture: ComponentFixture<RemoveTableButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemoveTableButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveTableButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
