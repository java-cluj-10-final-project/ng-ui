import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DeleteWarningDialogComponent } from '../../../warning-dialogs/components/delete-warning-dialog/delete-warning-dialog.component';

import DataType from '../../../../../constants/DataType';

@Component({
  selector: 'app-remove-table-button',
  templateUrl: './remove-table-button.component.html',
  styleUrls: ['./remove-table-button.component.css']
})
export class RemoveTableButtonComponent implements OnInit {

  @Input() type: string;
  @Input() deleteId: number;

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  public delete(): void {
    this.openDialog();
  }

  private openDialog(): void {
    const dialogRef = this.dialog.open(
      DeleteWarningDialogComponent,
      {
        data: {
          message:
            this.type === DataType.METER ?
              `The meter will be deleted` :
              `The meter ${this.type} will be deleted`,
          type: this.type,
          deleteId: this.deleteId
        }
      });

    dialogRef.afterClosed().subscribe(
      _ => {
        window.location.reload();
      });
  }

}
