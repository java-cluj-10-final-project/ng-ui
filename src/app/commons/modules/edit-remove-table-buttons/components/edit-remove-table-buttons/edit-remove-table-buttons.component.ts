import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MeterService } from 'src/app/modules/meters/services/meter.service';

@Component({
  selector: 'app-edit-remove-table-buttons',
  templateUrl: './edit-remove-table-buttons.component.html',
  styleUrls: ['./edit-remove-table-buttons.component.css']
})
export class EditRemoveTableButtonsComponent implements OnInit {

  @Input() type: string;
  @Input() editUrl: string;
  @Input() deleteId: number;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public edit(): void {
    this.router.navigateByUrl(this.editUrl);
  }
}
