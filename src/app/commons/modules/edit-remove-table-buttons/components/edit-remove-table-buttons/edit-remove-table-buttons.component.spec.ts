import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRemoveTableButtonsComponent } from './edit-remove-table-buttons.component';

describe('EditRemoveTableButtonsComponent', () => {
  let component: EditRemoveTableButtonsComponent;
  let fixture: ComponentFixture<EditRemoveTableButtonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditRemoveTableButtonsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRemoveTableButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
