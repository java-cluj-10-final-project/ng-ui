import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormErrorMessageComponent } from './components/form-error-message/form-error-message.component';



@NgModule({
  declarations: [FormErrorMessageComponent],
  imports: [
    CommonModule
  ],
  exports: [FormErrorMessageComponent]
})
export class ErrorMessagesModule { }
