import AccountPreview from './AccountPreview';

export default class MeterValue {
  id?: number;
  updateTime?: string;
  meterValue?: number;
  account?: AccountPreview;
}
