import MeterValue from './MeterValue';
import MeterInfo from './MeterInfo';
import AccountPreview from './AccountPreview';

export default class Meter {
  id?: number;
  name?: string;
  description?: string;
  iso?: string;
  account?: AccountPreview;
  values?: MeterValue[];
  infos?: MeterInfo[];
}
