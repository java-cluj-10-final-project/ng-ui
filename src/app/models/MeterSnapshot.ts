import MeterValue from './MeterValue';
import MeterInfo from './MeterInfo';
import AccountPreview from './AccountPreview';

export default class MeterSnaptshot {
  id?: number;
  name?: string;
  iso?: string;
  description?: string;
  account?: AccountPreview;
  value?: MeterValue;
}
