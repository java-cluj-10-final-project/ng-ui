import Meter from './Meter';
import Role from './Role';

export default class Account {
  email: string;
  createTime: string;
  updateTime: string;
  roles: Role[];
  meters: Meter[];
}
