export default class MeterInfo {
  id: number;
  key: string;
  value: string;
}
