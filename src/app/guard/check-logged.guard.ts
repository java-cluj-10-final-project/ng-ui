import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { TokenService } from '../services/token.service';

@Injectable({
  providedIn: 'root'
})
export class CheckLoggedGuard implements CanActivate {

  constructor(
    private service: TokenService,
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    if (this.service.getToken()) {
      return true;
    } else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }
}
