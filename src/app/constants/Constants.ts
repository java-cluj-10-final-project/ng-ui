export default class Constants {
    public static javaServerUrl: string = 'http://localhost:8080';

    public static loginApiPath: string = '/user/login';
    public static logoutApiPath: string = '/user/logout';
    public static registerApiPath: string = '/user/register';

    public static meterApiPath: string = '/api/v1/meter';
    public static meterValuePath: string = '/value';
    public static meterInfoPath: string = '/info';

    public static currentPage: string = 'View All Meters';
}
