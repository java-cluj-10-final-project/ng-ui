export default class DataType {
    public static METER: string = 'meter';
    public static INFO: string = 'info';
    public static VALUE: string = 'value';
}
