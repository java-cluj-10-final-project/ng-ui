import { Component, OnInit } from '@angular/core';
import Constants from 'src/app/constants/Constants';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  constructor(private service: TokenService) { }

  ngOnInit(): void {
    Constants.currentPage = 'Not Found';
  }

  public wreckToken(): void {
    this.service.saveToken('wrong token');
  }

}
