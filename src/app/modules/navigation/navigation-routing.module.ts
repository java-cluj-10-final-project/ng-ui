import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TopNavBarComponent } from './components/top-nav-bar/top-nav-bar.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NavigationRoutingModule { }
