import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatMenuModule } from '@angular/material/menu';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonModule } from '@angular/material/button';


import { NavigationRoutingModule } from './navigation-routing.module';
import { TopNavBarComponent } from './components/top-nav-bar/top-nav-bar.component';


@NgModule({
  declarations: [TopNavBarComponent],
  imports: [
    MatMenuModule,
    MatButtonToggleModule,
    MatButtonModule,
    CommonModule,
    NavigationRoutingModule
  ],
  exports: [
    TopNavBarComponent
  ]
})
export class NavigationModule { }
