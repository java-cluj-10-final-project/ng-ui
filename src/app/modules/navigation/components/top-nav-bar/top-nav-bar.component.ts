import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Constants from 'src/app/constants/Constants';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-top-nav-bar',
  templateUrl: './top-nav-bar.component.html',
  styleUrls: ['./top-nav-bar.component.css']
})
export class TopNavBarComponent implements OnInit {

  pageName = Constants;

  constructor(
    public token: TokenService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  public logout(): void {
    this.token.removeToken();
    this.router.navigate(['/login']);
  }
}
