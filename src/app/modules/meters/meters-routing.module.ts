import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CheckLoggedGuard } from 'src/app/guard/check-logged.guard';
import { MeterAddInfoComponent } from './components/meter-add-info/meter-add-info.component';
import { MeterAddComponent } from './components/meter-add/meter-add.component';
import { MeterEditInfoComponent } from './components/meter-edit-info/meter-edit-info.component';
import { MeterGridComponent } from './components/meter-grid/meter-grid.component';
import { MeterPageComponent } from './components/meter-page/meter-page.component';
import { MeterUpdateComponent } from './components/meter-update/meter-update.component';
import { UpdateMeterValueComponent } from './components/update-meter-value/update-meter-value.component';

const routes: Routes = [
  { path: 'meter', canActivate: [CheckLoggedGuard], component: MeterGridComponent, pathMatch: 'full', outlet: 'primary' },
  { path: 'meter/add', canActivate: [CheckLoggedGuard], component: MeterAddComponent, pathMatch: 'full', outlet: 'primary' },
  { path: 'meter/:id', canActivate: [CheckLoggedGuard], component: MeterPageComponent, pathMatch: 'full', outlet: 'primary' },
  { path: 'meter/:id/edit', canActivate: [CheckLoggedGuard], component: MeterUpdateComponent, pathMatch: 'full', outlet: 'primary' },
  {
    path: 'meter/:id/addValue',
    canActivate: [CheckLoggedGuard], component: UpdateMeterValueComponent, pathMatch: 'full', outlet: 'primary'
  },
  { path: 'meter/:id/addInfo', canActivate: [CheckLoggedGuard], component: MeterAddInfoComponent, pathMatch: 'full', outlet: 'primary' },
  {
    path: 'meter/:id/editInfo/:infoId', canActivate: [CheckLoggedGuard], component: MeterEditInfoComponent,
    pathMatch: 'full', outlet: 'primary'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MetersRoutingModule { }
