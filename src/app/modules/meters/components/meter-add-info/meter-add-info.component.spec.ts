import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeterAddInfoComponent } from './meter-add-info.component';

describe('MeterAddInfoComponent', () => {
  let component: MeterAddInfoComponent;
  let fixture: ComponentFixture<MeterAddInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeterAddInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeterAddInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
