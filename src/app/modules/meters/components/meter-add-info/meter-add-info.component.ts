import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Constants from 'src/app/constants/Constants';
import MeterInfo from 'src/app/models/MeterInfo';
import { MeterService } from '../../services/meter.service';

@Component({
  selector: 'app-meter-add-info',
  templateUrl: './meter-add-info.component.html',
  styleUrls: ['./meter-add-info.component.css']
})
export class MeterAddInfoComponent implements OnInit {

  addMeterForm: FormGroup;
  meterId: number;

  errorMessage: string;

  constructor(
    private route: ActivatedRoute,
    private service: MeterService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {

    this.route.params.subscribe(params => this.meterId = params.id, (_) => router.navigateByUrl('/meter'));
    this.addMeterForm = this.formBuilder.group({ key: '', value: '' });
  }

  ngOnInit(): void {
    Constants.currentPage = 'Add Meter Info';
  }

  addMeterInfo(form: any): any {

    const meterInfo: MeterInfo = new MeterInfo();

    meterInfo.key = form.key;
    meterInfo.value = form.value;

    this.service
      .addMeterInfo(this.meterId, meterInfo)
      .subscribe((response: any) => {
        console.log('Meter info created: ', response);
        this.router.navigateByUrl(`/meter/${this.meterId}`);
      },
        (err: any) => this.errorMessage = err.error.message);
  }
}
