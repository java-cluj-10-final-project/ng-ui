import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Constants from 'src/app/constants/Constants';
import Meter from 'src/app/models/Meter';
import MeterSnaptshot from 'src/app/models/MeterSnapshot';
import { MeterService } from '../../services/meter.service';

@Component({
  selector: 'app-meter-update',
  templateUrl: './meter-update.component.html',
  styleUrls: ['./meter-update.component.css']
})
export class MeterUpdateComponent implements OnInit {

  updateMeterForm: FormGroup = new FormGroup({
    name: new FormControl(),
    iso: new FormControl(),
    description: new FormControl()
  });
  id: number;

  errorMessage: string;

  constructor(
    private route: ActivatedRoute,
    private service: MeterService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {

    this.route.params.subscribe(
      params => {
        this.id = params.id;

        this.service.getMeter(params.id).subscribe(
          (meter: Meter) => {
            this.updateMeterForm = this.formBuilder.group({
              name: meter.name,
              iso: meter.iso,
              description: meter.description
            });
          },
          (err: any) => this.errorMessage = err.error.message);
      });
  }

  ngOnInit(): void {
    Constants.currentPage = 'Update Meter';
  }

  updateMeter(form: any): void {

    const meter: MeterSnaptshot = new MeterSnaptshot();

    meter.name = form.name;
    meter.iso = form.iso;
    meter.description = form.description;

    this.service
      .updateMeter(this.id, meter)
      .subscribe((response: any) => {
        console.log('Meter updated: ', response);
        this.router.navigateByUrl(`/meter`);
      },
        (err: any) => this.errorMessage = err.error.message);
  }
}
