import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateMeterValueComponent } from './update-meter-value.component';

describe('UpdateMeterValueComponent', () => {
  let component: UpdateMeterValueComponent;
  let fixture: ComponentFixture<UpdateMeterValueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateMeterValueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateMeterValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
