import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Constants from 'src/app/constants/Constants';
import AccountPreview from 'src/app/models/AccountPreview';
import MeterSnaptshot from 'src/app/models/MeterSnapshot';
import MeterValue from 'src/app/models/MeterValue';
import { MeterService } from '../../services/meter.service';

@Component({
  selector: 'app-update-meter-value',
  templateUrl: './update-meter-value.component.html',
  styleUrls: ['./update-meter-value.component.css']
})
export class UpdateMeterValueComponent implements OnInit {

  addMeterForm: FormGroup;
  id: number;

  errorMessage: string;

  constructor(
    private route: ActivatedRoute,
    private service: MeterService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {

    this.route.params.subscribe(params => this.id = params.id);
    this.addMeterForm = this.formBuilder.group({ value: 0 });
  }

  ngOnInit(): void {
    Constants.currentPage = 'Add New Meter Value';
  }

  updateMeter(form: any): any {

    const value: MeterValue = new MeterValue();
    value.meterValue = form.value;

    this.service
      .updateMeterValue(this.id, value)
      .subscribe((response: any) => {
        console.log('Meter value updated ', response);
        this.router.navigateByUrl(`/meter/${this.id}`);
      },
        (err: any) => this.errorMessage = err.error.message);
  }
}
