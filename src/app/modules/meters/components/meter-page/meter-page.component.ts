import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Constants from 'src/app/constants/Constants';
import AccountPreview from 'src/app/models/AccountPreview';
import Meter from 'src/app/models/Meter';
import MeterInfo from 'src/app/models/MeterInfo';
import MeterValue from 'src/app/models/MeterValue';
import { MeterService } from '../../services/meter.service';

@Component({
  selector: 'app-meter-page',
  templateUrl: './meter-page.component.html',
  styleUrls: ['./meter-page.component.css']
})
export class MeterPageComponent implements OnInit {

  meter: Meter = new Meter();

  valuesColumns: string[] = ['value', 'ISO', 'updated', 'actor', 'actions'];
  infosColumns: string[] = ['key', 'value', 'actions'];

  constructor(
    private route: ActivatedRoute,
    private service: MeterService,
    private router: Router
  ) {
    this.route.params.subscribe(
      params => {
        this.service
          .getMeter(params.id)
          .subscribe(
            (meter: Meter) => {
              this.meter = meter;
            },
            (err: any) => console.log(`Could not fetch the meter with id ${params.id}`)
          );
      },
      (err: any) => {
        console.log('Could not read the meter id from the path');
        this.router.navigateByUrl('/meter');
      });
  }

  account = new AccountPreview();
  meterValue = new MeterValue();
  meterInfo = new MeterInfo();

  ngOnInit(): void {
    Constants.currentPage = 'View Meter';
  }
}
