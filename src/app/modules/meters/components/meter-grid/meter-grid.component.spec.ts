import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeterGridComponent } from './meter-grid.component';

describe('MeterGridComponent', () => {
  let component: MeterGridComponent;
  let fixture: ComponentFixture<MeterGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeterGridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeterGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
