import { Component, OnInit, DoCheck } from '@angular/core';
import AccountPreview from 'src/app/models/AccountPreview';
import MeterSnapshot from 'src/app/models/MeterSnapshot';
import MeterInfo from 'src/app/models/MeterInfo';
import MeterValue from 'src/app/models/MeterValue';
import { MeterService } from '../../services/meter.service';
import Constants from 'src/app/constants/Constants';


@Component({
  selector: 'app-meter-grid',
  templateUrl: './meter-grid.component.html',
  styleUrls: ['./meter-grid.component.css']
})
export class MeterGridComponent implements OnInit {

  meters: MeterSnapshot[];

  constructor(private metersService: MeterService) {
  }

  account = new AccountPreview();
  meterValue = new MeterValue();
  meterInfo = new MeterInfo();

  ngOnInit(): void {
    Constants.currentPage = 'View All Meters';

    this.metersService
      .getMeters()
      .subscribe((data: MeterSnapshot[]) => {
        this.meters = data;
      });
  }
}
