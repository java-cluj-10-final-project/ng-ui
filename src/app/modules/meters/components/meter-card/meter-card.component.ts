import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DeleteWarningDialogComponent } from 'src/app/commons/modules/warning-dialogs/components/delete-warning-dialog/delete-warning-dialog.component';
import DataType from 'src/app/constants/DataType';
import MeterSnapshot from 'src/app/models/MeterSnapshot';
import { MeterService } from '../../services/meter.service';

@Component({
  selector: 'app-meter-card',
  templateUrl: './meter-card.component.html',
  styleUrls: ['./meter-card.component.css']
})
export class MeterCardComponent implements OnInit {

  @Input() meter: MeterSnapshot;

  errorMessage: string;

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  delete(): void {
    this.openDialog();
  }


  private openDialog(): void {
    const dialogRef = this.dialog.open(
      DeleteWarningDialogComponent,
      {
        data: {
          message: `The meter called ${this.meter.name} will be deleted`,
          type: DataType.METER,
          deleteId: this.meter.id
        }
      });

    dialogRef.afterClosed().subscribe(
      _ => {
        window.location.reload();
      });
  }
}
