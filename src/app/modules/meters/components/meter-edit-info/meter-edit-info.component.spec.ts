import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeterEditInfoComponent } from './meter-edit-info.component';

describe('MeterEditInfoComponent', () => {
  let component: MeterEditInfoComponent;
  let fixture: ComponentFixture<MeterEditInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeterEditInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeterEditInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
