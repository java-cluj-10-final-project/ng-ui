import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Constants from 'src/app/constants/Constants';
import Meter from 'src/app/models/Meter';
import MeterInfo from 'src/app/models/MeterInfo';
import { MeterService } from '../../services/meter.service';

@Component({
  selector: 'app-meter-edit-info',
  templateUrl: './meter-edit-info.component.html',
  styleUrls: ['./meter-edit-info.component.css']
})
export class MeterEditInfoComponent implements OnInit {

  editMeterForm: FormGroup = new FormGroup({
    key: new FormControl(),
    value: new FormControl()
  });

  meterId: number;
  meterInfoId: number;

  errorMessage: string;

  constructor(
    private route: ActivatedRoute,
    private service: MeterService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    Constants.currentPage = 'Edit Meter Info';

    this.route.params.subscribe(params => {
      this.meterId = params.id;
      this.meterInfoId = params.infoId;

      this.service.getMeter(this.meterId)
        .subscribe((meter: Meter) => {
          const info: MeterInfo = meter.infos.filter(instance => instance.id == this.meterInfoId).pop();
          this.editMeterForm = new FormGroup({
            key: new FormControl(info.key),
            value: new FormControl(info.value)
          });
        },
          (err: any) => {
            console.log(`Could not fetch meter information: ${err.error}`);
          });
    }, (_) => {
      console.log('Could not read the path parameters');
      this.router.navigateByUrl(`/meter`);
    });
  }

  editMeterInfo(form: any): any {

    const meterInfo: MeterInfo = new MeterInfo();
    meterInfo.id = this.meterInfoId;
    meterInfo.key = form.key;
    meterInfo.value = form.value;

    this.service
      .editMeterInfo(meterInfo)
      .subscribe((response: any) => {
        console.log('Meter info created: ', response);

        this.router.navigateByUrl(`/meter/${this.meterId}`);
      },
        (err: any) => this.errorMessage = err.error.message);
  }
}
