import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import Constants from 'src/app/constants/Constants';
import MeterSnaptshot from 'src/app/models/MeterSnapshot';
import MeterValue from 'src/app/models/MeterValue';
import { MeterService } from '../../services/meter.service';

@Component({
  selector: 'app-meter-add',
  templateUrl: './meter-add.component.html',
  styleUrls: ['./meter-add.component.css']
})
export class MeterAddComponent implements OnInit {

  addMeterForm;
  errorMessage;

  constructor(
    private service: MeterService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.addMeterForm = this.formBuilder.group({
      name: '',
      iso: '',
      description: '',
      value: 0
    });
  }

  ngOnInit(): void {
    Constants.currentPage = 'Add New Meter';
  }

  addMeter(form: any): any {

    const meter: MeterSnaptshot = new MeterSnaptshot();

    meter.name = form.name;
    meter.iso = form.iso;
    meter.description = form.description;

    this.service
      .addMeter(meter)
      .subscribe((response: any) => {
        console.log('Meter added: ', response);

        const value: MeterValue = new MeterValue();
        value.meterValue = form.value;
        this.service
          .updateMeterValue(response.id, value)
          .subscribe(
            (_: any) => {
              console.log('Meter value updated');
              this.router.navigateByUrl('/meter');
            }
            ,
            (error: any) => this.errorMessage = error.error.message
          );

      },
        (err: any) => {
          this.errorMessage = err.error.message;
        });
  }
}
