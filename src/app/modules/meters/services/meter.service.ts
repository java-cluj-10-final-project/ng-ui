import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Constants from '../../../constants/Constants';
import MeterSnaptshot from 'src/app/models/MeterSnapshot';
import { AuthorizationService } from 'src/app/services/authorization.service';
import MeterValue from 'src/app/models/MeterValue';
import MeterInfo from 'src/app/models/MeterInfo';

@Injectable({
  providedIn: 'root'
})
export class MeterService {


  constructor(private httpClient: HttpClient, private service: AuthorizationService) { }

  public getMeters(): any {
    const authHeader = this.service.getAuthorizationHeaders();
    const url = `${Constants.javaServerUrl}${Constants.meterApiPath}`;
    return this.httpClient.get(url, { headers: authHeader });
  }

  public getMeter(id: number): any {
    const authHeader = this.service.getAuthorizationHeaders();
    const url = `${Constants.javaServerUrl}${Constants.meterApiPath}/${id}`;
    return this.httpClient.get(url, { headers: authHeader });
  }

  public deleteMeter(id: number): any {
    const authHeader = this.service.getAuthorizationHeaders();
    const url = `${Constants.javaServerUrl}${Constants.meterApiPath}/${id}`;
    return this.httpClient.delete(url, { headers: authHeader });
  }

  public addMeter(meter: MeterSnaptshot): any {
    const authHeader = this.service.getAuthorizationHeaders();
    const url = `${Constants.javaServerUrl}${Constants.meterApiPath}`;
    return this.httpClient.post(url, meter, { headers: authHeader });
  }

  public updateMeter(id: number, meter: MeterSnaptshot): any {
    const authHeader = this.service.getAuthorizationHeaders();
    const url = `${Constants.javaServerUrl}${Constants.meterApiPath}/${id}`;
    return this.httpClient.patch(url, meter, { headers: authHeader });
  }

  public updateMeterValue(meterId: number, value: MeterValue): any {
    const authHeader = this.service.getAuthorizationHeaders();
    const url = `${Constants.javaServerUrl}${Constants.meterApiPath}/${meterId}${Constants.meterValuePath}`;
    return this.httpClient.post(url, value, { headers: authHeader });
  }

  public deleteMeterValue(id: number): any {
    const authHeader = this.service.getAuthorizationHeaders();
    const url = `${Constants.javaServerUrl}${Constants.meterApiPath}/${id}${Constants.meterValuePath}`;
    return this.httpClient.delete(url, { headers: authHeader });
  }

  public addMeterInfo(meterId: number, meterInfo: MeterInfo): any {
    const authHeader = this.service.getAuthorizationHeaders();
    const url = `${Constants.javaServerUrl}${Constants.meterApiPath}/${meterId}${Constants.meterInfoPath}`;
    return this.httpClient.post(url, meterInfo, { headers: authHeader });
  }

  public editMeterInfo(meterInfo: MeterInfo): any {
    const authHeader = this.service.getAuthorizationHeaders();
    const url = `${Constants.javaServerUrl}${Constants.meterApiPath}/${meterInfo.id}${Constants.meterInfoPath}`;
    return this.httpClient.patch(url, meterInfo, { headers: authHeader });
  }

  public deleteMeterInfo(id: number): any {
    const authHeader = this.service.getAuthorizationHeaders();
    const url = `${Constants.javaServerUrl}${Constants.meterApiPath}/${id}${Constants.meterInfoPath}`;
    return this.httpClient.delete(url, { headers: authHeader });
  }
}
