import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WarningDialogsModule } from './../../commons/modules/warning-dialogs/warning-dialogs.module';
import { MetersRoutingModule } from './meters-routing.module';
import { EditRemoveTableButtonsModule } from './../../commons/modules/edit-remove-table-buttons/edit-remove-table-buttons.module';
import { AddTableButtonModule } from './../../commons/modules/add-table-button/add-table-button.module';

import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MeterCardComponent } from './components/meter-card/meter-card.component';
import { MeterPageComponent } from './components/meter-page/meter-page.component';
import { MeterGridComponent } from './components/meter-grid/meter-grid.component';
import { MeterAddComponent } from './components/meter-add/meter-add.component';
import { MeterUpdateComponent } from './components/meter-update/meter-update.component';
import { UpdateMeterValueComponent } from './components/update-meter-value/update-meter-value.component';
import { ErrorMessagesModule } from 'src/app/commons/modules/error-messages/error-messages.module';
import { MeterAddInfoComponent } from './components/meter-add-info/meter-add-info.component';
import { MeterEditInfoComponent } from './components/meter-edit-info/meter-edit-info.component';


@NgModule({
  declarations: [
    MeterCardComponent,
    MeterPageComponent,
    MeterGridComponent,
    MeterAddComponent,
    MeterUpdateComponent,
    UpdateMeterValueComponent,
    MeterAddInfoComponent,
    MeterEditInfoComponent
  ],
  imports: [
    WarningDialogsModule,
    AddTableButtonModule,
    EditRemoveTableButtonsModule,
    ErrorMessagesModule,
    FormsModule, ReactiveFormsModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    HttpClientModule,
    CommonModule,
    MetersRoutingModule
  ]
})
export class MetersModule { }
