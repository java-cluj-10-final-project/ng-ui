import { HttpClient, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Constants from 'src/app/constants/Constants';
import { AuthorizationService } from 'src/app/services/authorization.service';
import User from '../../../models/User';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient, private authService: AuthorizationService) { }

  login(user: User): any {
    return this.httpClient.post(Constants.javaServerUrl + Constants.loginApiPath, user);
  }

  register(user: User): any {
    return this.httpClient.post(Constants.javaServerUrl + Constants.registerApiPath, user);
  }
}
