import { Component, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import User from 'src/app/models/User';
import { TokenService } from 'src/app/services/token.service';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  errorMessage: string;
  private redirectTo: string;

  constructor(
    private service: LoginService,
    private token: TokenService,
    public router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
  }

  login(form: NgForm): void {
    this.route.queryParams.subscribe(
      params => this.redirectTo = params.redirectTo ? params.redirectTo : '/meter'
    );

    const user = new User();
    user.username = form.value.email;
    user.password = form.value.password;

    this.service
      .login(user)
      .subscribe(
        (response: { token: string }) => {
          this.token.saveToken(response.token);
          this.router.navigateByUrl(this.redirectTo);
        },
        (err: any) => {
          this.errorMessage = err.error.message;
        });
  }

  toogle(): void {
    this.router.navigateByUrl('/register');
  }
}
