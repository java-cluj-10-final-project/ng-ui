import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import User from 'src/app/models/User';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  errorMessage: string;

  constructor(
    private service: LoginService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  register(form: NgForm): void {
    const user = new User();
    user.username = form.value.email;
    user.password = form.value.password;

    this.service.register(user)
      .subscribe(_ => this.toogle(),
        (err: any) => this.errorMessage = err.error.message);
  }

  toogle(): void {
    this.router.navigateByUrl('/login');
  }
}
