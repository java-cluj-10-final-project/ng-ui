import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './components/login/login.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';

import { FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { ErrorMessagesModule } from 'src/app/commons/modules/error-messages/error-messages.module';

@NgModule({
  declarations: [LoginFormComponent, RegisterFormComponent],
  imports: [
    ErrorMessagesModule,
    CommonModule,
    FormsModule,
    MatCardModule,
    LoginRoutingModule
  ],
  exports: [
    LoginFormComponent,
    RegisterFormComponent
  ]
})
export class LoginModule { }
