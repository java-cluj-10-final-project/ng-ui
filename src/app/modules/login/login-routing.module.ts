import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';

const routes: Routes = [
  { path: 'login', pathMatch: 'prefix', component: LoginFormComponent, outlet: 'primary' },
  { path: 'register', pathMatch: 'full', component: RegisterFormComponent, outlet: 'primary' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
