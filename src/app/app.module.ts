import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { LoginModule } from './modules/login/login.module';
import { NotFoundModule } from './modules/not-found/not-found.module';
import { MetersModule } from './modules/meters/meters.module';
import { NavigationModule } from './modules/navigation/navigation.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorInterceptorService } from './services/error-interceptor.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    LoginModule,
    MetersModule,
    NavigationModule,
    BrowserModule,
    AppRoutingModule,
    NotFoundModule,
    BrowserAnimationsModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptorService, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
