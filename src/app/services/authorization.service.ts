import { Injectable } from '@angular/core';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(private service: TokenService) { }

  public getAuthorizationHeaders(): any {
    const header = { Authorization: 'Bearer ' + this.service.getToken() };
    return header;
  }
}
